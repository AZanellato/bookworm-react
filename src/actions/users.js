import { USER_LOGGED_IN, USER_LOGGED_OUT } from '../types';
import api from '../api';



export const userLoggedIn = (user) => ({
    type: USER_LOGGED_IN,
    user
})
export const userLoggedOut = () => ({
    type: USER_LOGGED_OUT
})

export const signup = (data) => (dispatch) => 
    api.user.signup(data).then(user => {
        localStorage.bookwormJWT = user.token; 
        dispatch(userLoggedIn(user))
    });
